package memento;

import java.util.Random;

public class Riddler {

	static Random randGenerator = new Random();

	public static Memento joinGame() {
		int randomNumber = randGenerator.nextInt(10 + 1);
		return new Memento(randomNumber);
	}

	public static boolean guessRNumber(Memento m, int randomNumber) {

		if (m.getRandomNumber() == randomNumber) {
			System.out.println("Customer has guessed right and won");
			return true;
		}

		return false;
	}

}
