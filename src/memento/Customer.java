package memento;

import java.util.Random;

public class Customer implements Runnable {
	private RandomNumber randomNumber = new RandomNumber();
	private static Random r = new Random();
	private Riddler riddler;

	public Customer(Riddler riddler) {
		this.riddler = riddler;
	}

	@Override
	public void run() {

		int i;
		Boolean j = false;
		Memento m = Riddler.joinGame();

		while (!j) {
			i = r.nextInt(10 + 1);
			System.out.println("You guessed: " + i);
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			j = Riddler.guessRNumber(m, i);
		}

	}

	private static class RandomNumber {
		private static Memento m = Riddler.joinGame();
		private int rNumber;
	}

}
